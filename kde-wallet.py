from gi.repository import GObject
from gi.repository import Peas
from gi.repository import PeasGtk
from gi.repository import Gtk
from gi.repository import Liferea

from PyKDE4.kdeui import KWallet
from PyQt4 import QtGui

class KDEWalletPlugin(GObject.Object, Liferea.AuthActivatable):
	__gtype_name__ = 'KDEWalletPlugin'
	wallet = []

	object = GObject.property(type=GObject.Object)

	def do_activate(self):
		app = QtGui.QApplication([])
		self.wallet = KWallet.Wallet.openWallet(KWallet.Wallet.LocalWallet(), 0)
		if not self.wallet.hasFolder('liferea'):
			self.wallet.createFolder('liferea')
		self.wallet.setFolder('liferea')

	def do_deactivate(self):
		window = self.object

	def do_query(self, id):
		# Fetch secret by id
		key, qstr_password = self.wallet.readPassword(id)
		password_str = str(qstr_password)
		if password_str:
			username, password = password_str.split('@@@')
			Liferea.auth_info_from_store(id, username, password)

	def do_delete(self, id):
		self.wallet.removeEntry(id)

	def do_store(self, id, username, password):
		self.wallet.writePassword(id, '@@@'.join([username, password]))
